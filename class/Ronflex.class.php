<?php

class Ronflex extends Pokemon implements Comportement{

    private Type $type;
    private const CLASSNAME = "Ronflex";
    private int $niveau;
    private string $nom;

    public function __construct(string $nom, int $niveau)
    {
        parent::__construct($nom, $niveau,new TypeNormal()); 
        $this->appear($nom,$niveau);
    }

    public function attaque(Pokemon $pokemon) :void {
        echo "<br>{$this->getNom()} ATTAQUE {$pokemon->getNom()}";
       
        if($this->getNiveau()>= $pokemon->getNiveau()) {
            $pokemon->fuite();
        }else {
            echo "<br>{$this->getNom()} SE FAIT DEMOLIR PAR {$pokemon->getNom()}";
            $this->fuite();
        }
    }

    private function appear(string $nom, int $niveau)
    {
        echo "<br><br>Un nouveau " . self::CLASSNAME . " de niveau {$niveau} apparaît, il s'appelle {$nom} !";
    }

    public function fuite() : void {
        echo "<br>{$this->getNom()} FUIT";
    }

    public function dort() : void {
        echo "<br>{$this->getNom()} DORT";
    }

    public function getClass() : string
    {
        return self::CLASSNAME;
    }
}