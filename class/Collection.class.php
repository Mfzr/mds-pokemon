<?php

class Collection {
    /* It's a private property of the class Collection. */
    /**
     * @var array<Pokemon>
     */
    private array $col;

    public function __construct()
    {
        $this->col = [];
    }

    public function addPokemon(Pokemon $pokemon){
        $this->col[] = $pokemon;
    }

    public function getCollection()
    {
        return $this->col;
    }
    public function getFromCollection(string $nom) : Pokemon
    {
        foreach( $this->col as $poke) {
            
            if($poke->getNom() === $nom) {
                return $poke;
            }

        }
    }
}
