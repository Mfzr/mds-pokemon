<?php

abstract class Pokemon {
    private string $nom;
    private int $niveau;
    private Type $type;

    protected function __construct(string $nom, string $niveau, Type $type)
    {
        $this->setNom($nom)->setNiveau($niveau)->setType($type);
       

    }

    private function setNom(string $nom) : self
    {
        $this->nom = $nom;
        return $this;
    }

    public function getNom() : string 
    {
        return $this->nom;
    }


    private function setNiveau(int $niveau) : self
    {
        $this->niveau = $niveau;
        return $this;
    }

    public function getNiveau() : int
    {
        return $this->niveau;
    }

    private function setType(Type $type) : self
    {
        $this->type = $type;
        return $this;
    }

    abstract public function getClass() : string;
    abstract public function attaque(Pokemon $pokemon) : void;
    abstract public function fuite() : void;
    abstract public function dort() : void;

}