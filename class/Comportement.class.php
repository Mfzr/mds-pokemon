<?php

interface Comportement{
    public function attaque(Pokemon $pokemon) : void;
    public function fuite() : void;
    public function dort() : void;
}