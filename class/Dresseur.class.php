<?php


class Dresseur {
    private string $nom;
    private Collection $collection;

    public function __construct(string $nom)
    {
        $this->setNom($nom);
        $this->collection = new Collection();
        echo "<br><br>Un nouveau dresseur arrive, il s'apelle {$nom}";
    }

    private function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function catchPokemon(Pokemon $pokemon)
    {
        $this->collection->addPokemon($pokemon);
        echo "<br><br>Bravo {$this->nom}, tu as attrapé un {$pokemon->getClass()} de niveau {$pokemon->getNiveau()}, il s'apelle {$pokemon->getNom()}";
    }

    public function getCollection() : Collection
    {
        return $this->collection;
    }
}