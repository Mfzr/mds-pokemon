<?php

abstract class Type{
    private string $type;

    protected function __construct(string $type)
    {
        $this->setType($type);
    }

    private function setType(string $type) : self
    {
        $this->type = $type;
        return $this;
    }

    public function getType() : string 
    {
        return $this->type;
    }

    
}